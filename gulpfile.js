"use strict";

const gulp       = require("gulp");
const typescript = require("gulp-typescript");
const del        = require("del");
const seq        = require("run-sequence");
const browserify = require("browserify");
const source     = require("vinyl-source-stream");

/**
 * Deletes the build folder.
 */
gulp.task("clean", function() {
	return del(["./build"]);
});

/*
 * Copies all EJS templates to the build folder.
 */
gulp.task("build:ejs", function() {
  return gulp.src(["./src/**/*.ejs"])
		.pipe(gulp.dest("./build"));
});

/**
 * Compiles all TypeScript code into the build folder. All TypeScript 
 * annotations are removed during compilation.
 */
gulp.task("build:typescript", function() {
	const project = typescript.createProject("./tsconfig.json");
	return project.src()
		.pipe(typescript(project))
		.pipe(gulp.dest("./build"));
});

/**
 * Compiles all React code into a single Javascript file that can be executed
 * by the client. This must be done after compiling TypeScript.
 */
gulp.task("build:react", function() {
  return browserify({
    entries: ["./build/views/index.js"],
    extensions: [".js"],
    debug: true
  }).bundle()
  	.pipe(source("index.js"))
		.pipe(gulp.dest("./build/public/js"));
});

/**
 * Run all build tasks after cleaning the build folder.
 */
gulp.task("build", function(done) {
  seq("clean", ["build:ejs", "build:typescript"], "build:react", done);
});

/**
 * Default gulp task.
 */
gulp.task("default", ["build"], function() {
});