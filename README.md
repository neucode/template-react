# React Template

Mirror this repository and customise it to your needs.

## Get Started

+ Install node
+ Install global packages `npm install -g gulp typescript typings`
+ Install npm packages `npm install`
+ Install type definitions `typings install --global`
+ Run `npm start`

## Contributors

+ Benjamin Wang