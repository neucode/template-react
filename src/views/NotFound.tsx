/// <reference path="../../typings/index.d.ts"/>

import * as React from "react";
import * as HasDispatcher from "./attachments/HasDispatcher";
import {Dispatcher} from "../dispatcher/Dispatcher";

interface IProps {
}

interface IState {
}

export function BuildNotFound(dispatcher: Dispatcher) {
  
  class NotFound extends React.Component<HasDispatcher.IProps & IProps, IState> {

    constructor(props: HasDispatcher.IProps & IProps) {
      super(props);
      this.state = {};
    }

    public render(): JSX.Element {
      return (
        <div>Not Found!</div>
      );
    }

  }

  return HasDispatcher.Attach<IProps>(NotFound, dispatcher);
}