/// <reference path="../../typings/index.d.ts"/>

import * as React from "react";
import * as HasDispatcher from "./attachments/HasDispatcher";
import {Dispatcher} from "../dispatcher/Dispatcher";

interface IProps {
}

interface IState {
}

export function BuildHome(dispatcher: Dispatcher) {
  
  class Home extends React.Component<HasDispatcher.IProps & IProps, IState> {

    constructor(props: HasDispatcher.IProps & IProps) {
      super(props);
      this.state = {};
    }

    public render(): JSX.Element {
      return (
        <div>Hello, World!</div>
      );
    }

  }

  return HasDispatcher.Attach<IProps>(Home, dispatcher);
}