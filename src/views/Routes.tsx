/// <reference path="../../typings/index.d.ts"/>

import * as React from "react";
import {Route} from "react-router";
import {Dispatcher} from "../dispatcher/Dispatcher";
import {BuildHome} from "./Home";
import {BuildNotFound} from "./NotFound";

export function BuildRoutes(dispatcher: Dispatcher) {

  const Home = BuildHome(dispatcher);
  const NotFound = BuildNotFound(dispatcher);

  return (
    <Route>
      <Route path="/" component={Home} />
      <Route path="*" component={NotFound} />
    </Route>
  );
}