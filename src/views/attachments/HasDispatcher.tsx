/// <reference path="../../../typings/index.d.ts"/>

import * as React from 'react';
import {Dispatcher} from '../../dispatcher/Dispatcher';

/**
 * IProps declares all props that a React.ComponentClass is required to expose 
 * in order to have a Dispatcher attached.
 */
export interface IProps {
  dispatcher: Dispatcher
}

/**
 * Injects a Dispatcher into the props of a React.ComponentClass. All 
 * components created from this class will have access to the dispatcher.
 * 
 * @param Component  - The React.ComponentClass that exposes props compatible
 *                     with IProps.
 * @param dispatcher - The Dispatcher that will be injectd into the given
 *                     React.ComponentClass.
 * @return           - A new React.ComponentClass that can be instantiated  
 *                     into JSX.Element components. These instances will have
 *                     to the given Dispatcher.
 */
export function Attach<T>(Component: React.ComponentClass<IAttachProps & IProps & T>, dispatcher: Dispatcher): React.ComponentClass<IAttachProps & T> {
  return class extends React.Component<IAttachProps & T, IAttachState> {
    public render(): JSX.Element {
      return (
        <Component {...this.props} dispatcher={dispatcher} />
      );
    }
  }
}

/**
 * Internal props used by the attachment.
 */
interface IAttachProps {
}

/**
 * Internal state used by the attachment.
 */
interface IAttachState {
}