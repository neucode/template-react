/// <reference path="../../typings/index.d.ts"/>

import * as React from "react";
import * as ReactDOM from "react-dom";
import {Router, browserHistory} from "react-router";

import * as HasDispatcher from "./attachments/HasDispatcher";
import {Dispatcher} from "../dispatcher/Dispatcher";
import {BuildRoutes} from "./Routes";

interface IProps {
}

interface IState {
}

export function BuildRouter(dispatcher: Dispatcher) {
  
  class R extends React.Component<HasDispatcher.IProps & IProps, IState> {

    constructor(props: HasDispatcher.IProps & IProps) {
      super(props);
      this.state = {};
    }

    public render(): JSX.Element {
      return (
        <Router history={browserHistory}>
          {BuildRoutes(dispatcher)}
        </Router>
      );
    }

  }

  return HasDispatcher.Attach<IProps>(R, dispatcher);
}

ReactDOM.render(
  React.createElement(BuildRouter(new Dispatcher())), 
  document.getElementById('react-root')
);
