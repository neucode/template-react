/// <reference path="../typings/index.d.ts"/>

import * as express from "express";
import * as React from "react";
import * as ReactDOM from "react-dom/server";
import {RouterContext, match} from "react-router";
import {Dispatcher} from "./dispatcher/Dispatcher";
import {BuildRoutes} from "./views/Routes";

interface Request extends express.Request {
  dispatcher: Dispatcher;
}

interface Response extends express.Response {
}

/**
 * This middleware will add a dispatcher to the request. It can be used by
 * subsequent middleware to dispatch actions and create views.
 */
export function dispatcher(): express.RequestHandler {
  return (req: Request, res: Response, next: express.NextFunction) => {
    req.dispatcher = new Dispatcher();
    next();
  }
}

/**
 * Server side routing using react router. Adding new routes is done in the
 * views.
 */
export function routes(): express.RequestHandler {
  return (req: Request, res: Response) => {
    match({ routes: BuildRoutes(req.dispatcher), location: req.url }, (error, redirect, props) => {
      if (error) {
        res.status(500).send(error.message);
      } else if (redirect) {
        res.redirect(302, redirect.pathname + redirect.search);
      } else if (props) {
        res.render("index.ejs", {
          reactState: req.dispatcher.renderState(),
          reactRoot: ReactDOM.renderToString(React.createElement(RouterContext, props as any))
        });
      } else {
        res.status(404).send("Not found");
      }
    });
  };
}