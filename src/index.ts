/// <reference path="../typings/index.d.ts"/>

import * as express from "express";
import * as path from "path";
import {dispatcher, routes} from "./routes";

// Configure required middleware
const jsx = require("node-jsx").install({ harmony: true, extension: ".jsx" });
const app = express();

// Configure view engine
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// Configure the middleware and routes
app.use(dispatcher());
app.use(routes());

// Listen
const server = app.listen(process.env.PORT || 5000, () => {
  const port = server.address().port;
  console.log('Listening on port ' + port);
});
