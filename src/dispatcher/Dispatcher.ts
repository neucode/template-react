/// <reference path="../../typings/index.d.ts"/>

import * as Flux from "flux";
import * as FluxUtils from "flux/utils";
import * as Immutable from "immutable";

/**
 * @class Dispatcher
 * 
 * An isomorphic dispatcher that can render and restore state to and from
 * strings. All stores are lazily evaluated to increase efficiency on the 
 * server. 
 * 
 * On the server, each request should create a new dispatcher that is rendered
 * to a string at the end of processing the request. This string should be 
 * passed to the client so that the client can bootstrap from this state.
 * 
 * On the client, there should be exactly one dispatcher. Using this class it
 * will automatically try and bootstrap from the state rendered by the server.
 */
export class Dispatcher extends Flux.Dispatcher<any> {

  private initialState: any;
  private stores: Immutable.Map<string, FluxUtils.ReduceStore<any>>;

  /**
   * Create an empty map of stores and get the state that was rendered by the
   * server.
   */
  constructor() {
    super();
    this.stores = Immutable.Map<string, FluxUtils.ReduceStore<any>>();
    if (typeof window !== "undefined") {
      this.initialState = window["initialState"];
    }
  }

   /**
    * @return - The state as rendered by the server. This value will be null 
    *           on the server. This can be used by stores to bootstrap their
    *           state.
    */
  public getInitialState(): any {
    return this.initialState;
  }

  /**
   * Render the current state of the dispatcher and all stores. This can be
   * used by the server to pass the state to the client.
   * 
   * @return - The current state rendered as a string.
   */
  public renderState(): string {
    return "window.initialState={" + this.stores.map((store, key) => {
        return key + ":" + JSON.stringify(store.getState()) + ',';
      }) + "};";
  }

}